# ZSL

# How to edit install server crontab to point to install-setup repo in gitlab

## SSH to server with ip 10.200.11.2 with user test. I guess anybody knows the password :P

```bash
[18:50:26][nix@t61:~]$ ssh test@10.200.11.2
test@10.200.11.2's password: 
Welcome to Ubuntu 18.04.2 LTS (GNU/Linux 4.15.0-65-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

 * Kata Containers are now fully integrated in Charmed Kubernetes 1.16!
   Yes, charms take the Krazy out of K8s Kata Kluster Konstruction.

     https://ubuntu.com/kubernetes/docs/release-notes

 * Canonical Livepatch is available for installation.
   - Reduce system reboots and improve kernel security. Activate at:
     https://ubuntu.com/livepatch
Last login: Tue Oct 22 09:15:58 2019 from 10.200.11.102
test@office-pxe:~$ 
```

## Became root with sudo su -

```bash
test@office-pxe:~$ sudo su -
root@office-pxe:~#
```

## rm current git directory /var/www/html/vnbs and create /var/www/html/install-setup. Also check :)

```bash
root@office-pxe:~# rm -rf /var/www/html/vnbs
root@office-pxe:~# ls -lat /var/www/html/
total 12
drwxr-xr-x 3 root root 4096 Nov  2 19:00 .
drwxr-xr-x 3 root root 4096 Oct 19 17:44 games
drwxr-xr-x 4 root root 4096 Sep 22 12:20 ..
root@office-pxe:~# 
```

## Then do git clone

```bash
root@office-pxe:~# cd /var/www/html/install-setup/
root@office-pxe:/var/www/html/install-setup# cd -
/root
root@office-pxe:~# rm -rf /var/www/html/install-setup/
root@office-pxe:~# cd  /var/www/html/
root@office-pxe:/var/www/html# git clone https://gitlab.com/zaselata/computer-cabinets/install-setup.git
Cloning into 'install-setup'...
remote: Enumerating objects: 546, done.
remote: Counting objects: 100% (546/546), done.
remote: Compressing objects: 100% (254/254), done.
remote: Total 546 (delta 278), reused 538 (delta 270)
Receiving objects: 100% (546/546), 519.33 KiB | 117.00 KiB/s, done.
Resolving deltas: 100% (278/278), done.
root@office-pxe:/var/www/html# 
```

## Cat /etc/crontab and change the repo with your favorite editor VIM :) . In my case I will switch from vnbs repo to install-setup

```bash
root@office-pxe:~# cat /etc/crontab 
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user	command
17 *	* * *	root    cd / && run-parts --report /etc/cron.hourly
25 6	* * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6	* * 7	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6	1 * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
*/1 * * * * root	cd /var/www/html/vnbs/ && git pull -q https://github.com/nix-bg/vnbs.git
#
root@office-pxe:~# 
```

```bash
root@office-pxe:~# cat /etc/crontab 
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user	command
17 *	* * *	root    cd / && run-parts --report /etc/cron.hourly
25 6	* * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6	* * 7	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6	1 * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
*/1 * * * * root	cd /var/www/html/install-setup/ && git pull -q https://gitlab.com/zaselata/computer-cabinets/install-setup.git
#
root@office-pxe:~# 
```
## Then you should edit this file /var/lib/tftpboot/bionic/ubuntu-installer/amd64/boot-screens/txt.cfg and search and replace vnbs with install-setup

```bash
root@office-pxe:~# cat /var/lib/tftpboot/bionic/ubuntu-installer/amd64/boot-screens/txt.cfg
default autodesktop
label autodesktop
	menu label ^Autoinstall Desktop
	menu default
	kernel ubuntu-installer/amd64/linux
	append vga=788 initrd=ubuntu-installer/amd64/initrd.gz auto=true locale=en_US.UTF-8 keyboard-configuration/layoutcode=us url=http://10.200.11.2/vnbs/pxe/preseed-desktop.txt --- quiet 

label autoserver
	menu label ^Autoinstall Server
	kernel ubuntu-installer/amd64/linux
	append vga=788 initrd=ubuntu-installer/amd64/initrd.gz auto=true locale=en_US.UTF-8 keyboard-configuration/layoutcode=us url=http://10.200.11.2/vnbs/pxe/preseed-server.txt --- quiet 

label install
	menu label ^Install
	kernel ubuntu-installer/amd64/linux
	append vga=788 initrd=ubuntu-installer/amd64/initrd.gz --- quiet 

label cli
	menu label ^Command-line install
	kernel ubuntu-installer/amd64/linux
	append tasks=standard pkgsel/language-pack-patterns= pkgsel/install-language-support=false vga=788 initrd=ubuntu-installer/amd64/initrd.gz --- quiet 
root@office-pxe:~# 

```
## To became

```bash
root@office-pxe:~# cat /var/lib/tftpboot/bionic/ubuntu-installer/amd64/boot-screens/txt.cfg
default autodesktop
label autodesktop
	menu label ^Autoinstall Desktop
	menu default
	kernel ubuntu-installer/amd64/linux
	append vga=788 initrd=ubuntu-installer/amd64/initrd.gz auto=true locale=en_US.UTF-8 keyboard-configuration/layoutcode=us url=http://10.200.11.2/install-setup/pxe/preseed-desktop.txt --- quiet 

label autoserver
	menu label ^Autoinstall Server
	kernel ubuntu-installer/amd64/linux
	append vga=788 initrd=ubuntu-installer/amd64/initrd.gz auto=true locale=en_US.UTF-8 keyboard-configuration/layoutcode=us url=http://10.200.11.2/install-setup/pxe/preseed-server.txt --- quiet 

label install
	menu label ^Install
	kernel ubuntu-installer/amd64/linux
	append vga=788 initrd=ubuntu-installer/amd64/initrd.gz --- quiet 

label cli
	menu label ^Command-line install
	kernel ubuntu-installer/amd64/linux
	append tasks=standard pkgsel/language-pack-patterns= pkgsel/install-language-support=false vga=788 initrd=ubuntu-installer/amd64/initrd.gz --- quiet 
root@office-pxe:~# 
```

## Thats all folks now every installation will start from gitlab install-setup repo